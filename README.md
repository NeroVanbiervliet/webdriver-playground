# Webdriver playground

This is an starter project to play around with (Selenium) [Webdriver](https://www.selenium.dev/).



### Setting up

Easy peasy: `pipenv install`

If you don't have [pipenv](https://pipenv.pypa.io/en/latest/) (boo!), try `pip install selenium webdriver-manager`



### Running

Running the test scenario

- `pipenv run python main.py`
- or `python main.py` if you did not use pipenv



Run the REPL

- `pipenv run ./repl.py`
- `python repl.py` if you did not use pipenv


### Assignments

1. **Get past the "register" dialog**

   Enter your name and click the "save" button.

   Tips:
   ```python
    driver.find_element_by_css_selector('.some-class #some-id')
    element.send_keys()
    element.click()
    ```

2. **Add a random number of tabs (between two and five)**

3. **Upload a csv file and import it**

   Use this one: [demo.csv](demo.csv)

4. **Visualise a signal**

5. **Create a screenshot**

    Visualisations are beautiful. Lets get you a new wallpaper ;)
