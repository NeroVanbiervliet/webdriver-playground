from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

import config


def get_local_driver():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    url = 'https://{}:{}@{}'.format(config.BASIC_AUTH_ACC, config.BASIC_AUTH_PASS, config.HOST)
    driver.get(url)
    return driver


def get_driver():
    driver = get_local_driver()
    driver.set_window_size(1920, 1080)
    driver.implicitly_wait(config.TIMEOUT)  # keeps retrying if DOM element not found
    return driver
