from setup import get_driver


def main():
    driver = get_driver()
    run_scenario(driver)
    driver.quit()


def run_scenario(driver):
    # implement the required tests here
    # use asserts to do checks
    assert False


if __name__ == '__main__':
    main()
