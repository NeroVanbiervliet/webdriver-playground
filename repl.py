import platform

from code import InteractiveConsole

if platform.system() == 'Linux':
    import readline  # gives history

from setup import get_driver

driver = get_driver()
scope_vars = {'d': driver, 'driver': driver}
InteractiveConsole(locals=scope_vars).interact("Repl away, use driver or d", "Bye")
driver.quit()
